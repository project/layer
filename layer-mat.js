
(function($) {
	Drupal.behaviors.layer = {
		attach: function (context) {		
			var msg = jQuery('#layer_message_wrapper').html();
      if (msg) {
        layer.alert(msg);
      }			
		}
	}
})(jQuery);
