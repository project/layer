<?php

/*
 * Admin settings menu callback
 */
function layer_settings_form() {
	 $settings = layer_get_settings();
  $path = drupal_get_path('module', 'layer');

  $form['override'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Override drupal message'),
    '#default_value' => $settings['override'],
  );
  
  $form['visibility'] = array(
    '#type'          => 'radios',
    '#title'         => t('Exclude/Include for specified pages'),
    '#options'       => array(0 => 'exlcude', 1 => 'include'),
    '#default_value' => $settings['visibility'],
  );
  
  $form['visible_pages'] = array(
    '#type'  => 'textarea',
    '#title' => t('Include/Exclude pages'),
    '#default_value' => $settings['visible_pages'],
    '#description'   => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
  );
  
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['#theme']    = 'system_settings_form';
  $form['#submit'][] = 'layer_form_submit';
  return $form;
}

function layer_form_submit($form, &$form_state) {
  $settings = layer_get_settings();
  $submitted   = array('override', 'visibility', 'visible_pages');
  
  foreach ($form_state['values'] as $key => $val) {
    if (in_array($key, $submitted)) {
      $settings[$key] = $val;
    }
  }
  
  variable_set('layer_settings', $settings);
  drupal_set_message(t('The configuration options have been saved.'));
  cache_clear_all();
}
